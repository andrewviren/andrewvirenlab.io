---
title: "Home"
---

# Hello
# About me

I am working to help people to get the most out of their jobs.

To do that they inherently need to change how they work.

To be able to change they need to be able to learn.

To learn at one person likely needs to use a learning system. For many to learn, they must be able to use a learning systems.

All systems at work must be learned.

The selection and implementation of systems has to be done with intentionality to help people learn.

I'm here to help with all of this.
